package com.servletFiles;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import com.classFiles.Request;
import com.dao.RequestDao;

@WebServlet("/contactus")
public class ContactUsServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//request.getRequestDispatcher("contactus.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher redirectClient = request.getRequestDispatcher("contactus.jsp");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String message = request.getParameter("message");
		
		Request setData = new Request();
		setData.setName(name);
		setData.setEmail(email);
		setData.setPhone(phone);
		setData.setMessage(message);
		
		try {
			String saveStatusMessage = "Server Error. Please! Try again later.";
			int status = RequestDao.saveRequestQuery(setData);
			if(status > 0) {
				saveStatusMessage = "Thanks for your query. We'll get back to you shortly!";
			}
			request.setAttribute("showMessage", saveStatusMessage);
			redirectClient.forward(request, response);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
