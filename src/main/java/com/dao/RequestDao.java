package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.classFiles.Request;

public class RequestDao {
	private static String URL = "jdbc:postgresql://localhost:5432/guest";
	private static String USER = "guest";
	private static String PASSWORD = "guest123";
	static Connection con = null;
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.postgresql.Driver");
		 con = DriverManager.getConnection(URL, USER, PASSWORD);
		return con;
	}
	
	public static int saveRequestQuery(Request saveData) throws ClassNotFoundException, SQLException {
		int status = 0;
		
		con = RequestDao.getConnection();
		String saveQuery = "insert into contactus values(?,?,?,?,?)";
		PreparedStatement queryStatement = con.prepareStatement(saveQuery);
		queryStatement.setString(1, saveData.getName());
		queryStatement.setString(2, saveData.getEmail());
		queryStatement.setString(3, saveData.getPhone());
		queryStatement.setString(4, saveData.getMessage());
		queryStatement.setBoolean(5, true);
		status = queryStatement.executeUpdate();
		
		return status;
	}
}
