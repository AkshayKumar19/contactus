package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
	public static boolean check(String user, String pass) throws ClassNotFoundException, SQLException {
		
		Connection con = RequestDao.getConnection();
		String checkQuery = "select * from login where uname = ? and pass = ?";
		PreparedStatement executeStatement = con.prepareStatement(checkQuery);
		executeStatement.setString(1, user);
		executeStatement.setString(2, pass);
		ResultSet loginData = executeStatement.executeQuery();
		if(loginData.next()) {
			return true;
		}
		return false;
	}
}
